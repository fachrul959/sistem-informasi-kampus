@extends('layouts.main')

@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Create New Data</h1>
</div>
<div class="col-lg-8">
    <form method="POST" action="/pendidikan">
        @csrf
        <div class="mb-3">
            <label for="nip" class="form-label">NIP Dosen</label>
            <input type="text" name="dosen_NIP" class="form-control" id="nip" required
                   value="@if($dosen_NIP != 0){{ $dosen_NIP }}@endif">
        </div>
        <div class="mb-3">
            <label for="strata" class="form-label">Strata</label>
            <input type="text" name="strata" class="form-control" id="strata" required>
        </div>
        <div class=" mb-3">
            <label for="jurusan" class="form-label">Jurusan</label>
            <input type="text" name="jurusan" class="form-control" id="jurusan" required>
        </div>
        <div class="mb-3">
            <label for="sekolah" class="form-label">Sekolah</label>
            <input type="text" name="sekolah" class="form-control" id="sekolah" required>
        </div>
        <div class=" mb-3">
            <label for="tahun_mulai" class="form-label">Tahun Mulai</label>
            <input type="text" name="tahun_mulai" class="form-control" id="tahun_mulai" required>
        </div>
        <div class=" mb-3">
            <label for="tahun_selesai" class="form-label">Tahun Selesai</label>
            <input type="text" name="tahun_selesai" class="form-control" id="tahun_selesai" required>
        </div>
        <button type=" submit" class="btn btn-primary">Create Data</button>
    </form>
</div>
@endsection
