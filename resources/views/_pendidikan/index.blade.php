@extends('layouts.main')
@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Riwayat Pendidikan</h1>
</div>

@if (session()->has('success'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<div class="table-responsive col-lg-8">
    <a href="/pendidikan/{{ $dosen_NIP }}/create" class="btn btn-primary mb-3">Create New Data</a>
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th scope="col">NIP Dosen</th>
                <th scope="col">Strata</th>
                <th scope="col">Jurusan</th>
                <th scope="col">Sekolah</th>
                <th scope="col">Tahun Mulai</th>
                <th scope="col">Tahun Selesai</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pendidikan as $item)
            <tr>
                <td>{{ $item['dosen_NIP'] }}</td>
                <td>{{ $item['strata'] }}</td>
                <td>{{ $item['jurusan'] }}</td>
                <td>{{ $item['sekolah'] }}</td>
                <td>{{ $item['tahun_mulai'] }}</td>
                <td>{{ $item['tahun_selesai'] }}</td>
                <td>
                    <a href="/pendidikan/{{ $item['id'] }}/edit" class="badge bg-warning"><span
                              data-feather="edit"></span></a>
                    <form method="post" action="/pendidikan/{{ $item['id'] }}" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="badge bg-danger border-0" onclick="return confirm('Are you sure?')"><span
                                  data-feather="trash"></span></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
